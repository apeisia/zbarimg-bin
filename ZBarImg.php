<?php

namespace Apeisia\ZBarImg;

use Imagick;
use Symfony\Component\Process\Process;

abstract class ZBarImg
{
    public static function readCodes(Imagick $image)
    {
        $white = new Imagick();
        $white->newImage($image->getImageWidth(), $image->getImageHeight(), "white");
        $white->compositeimage($image, Imagick::COMPOSITE_OVER, 0, 0);

        $tmp    = tempnam(sys_get_temp_dir(), 'qrcode');
        $pixels = $white->exportImagePixels(0, 0, $white->getImageWidth(), $white->getImageHeight(), 'I', imagick::PIXEL_CHAR);
        file_put_contents($tmp, pack('SSc*', $image->getImageWidth(), $image->getImageHeight(), ...$pixels));

        $p = new Process(['./zbarimg', '--raw', '-q', $tmp], __DIR__);
        $p->run();
        @unlink($tmp);
        return trim($p->getOutput());
    }

}